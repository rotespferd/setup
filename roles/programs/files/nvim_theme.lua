return {
	{ "shaunsingh/nord.nvim" },
	{
		"lazyvim/lazyvim",
		opts = {
			colorscheme = "nord",
		},
	},
}
