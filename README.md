# Setup
A collection of Ansible roles and playbooks to install my clients.

## Before run
Install Ansible on the host which should run the playbook via dnf or other ways.

Install needed Ansible modules on the host which should run the playbooks:
- ansible-galaxy collection install community.general
- ansible-galaxy collection install kewlfft.aur

## How to run
You can run a playbook via the command `ansible-playbook -K -i inventory $PLAYBOOKNAME.yml`. There are currently 2 playbooks: 'laptop' and 'workstation'.

There are also many tags to only run parts of the playbook. To run parts of the playbook via tags you can use the command `ansible-playbook -K -i inventory $PLAYBOOKNAME.yml --tags "$TAG1, $TAG2"`.
